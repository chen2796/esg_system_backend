from django.db import models

# Create your models here.


#环境保护
class EnvironmentalData(models.Model):
    # # 序号选择项
    # SERIAL_CHOICES = [(i, str(i)) for i in range(1, 12)]

    # 序号，作为主键
    serial = models.IntegerField(primary_key=True, verbose_name="序号")
    # 股票简称
    stock_name = models.CharField(max_length=100, verbose_name="股票简称")
    # 股票代码
    stock_code = models.CharField(max_length=10, verbose_name="股票代码")
    # 报告期
    report_period = models.CharField(max_length=10, verbose_name="报告期")
    # 碳排放总量
    carbon_emission_total = models.DecimalField(max_digits=20, decimal_places=2,
                                                verbose_name="碳排放总量(范围一、二)(吨CO2e)")
    # 单位营收碳排放量
    carbon_emission_per_revenue = models.DecimalField(max_digits=20, decimal_places=2,
                                                      verbose_name="单位营收碳排放量(范围一、二)(吨CO2e/百万元)")
    # 用水量
    water_usage = models.DecimalField(max_digits=20, decimal_places=2, verbose_name="用水量")
    # 单位营收用水量
    water_usage_per_revenue = models.DecimalField(max_digits=20, decimal_places=2, verbose_name="单位营收用水量")
    # 废弃物总量
    waste_total = models.DecimalField(max_digits=20, decimal_places=2, verbose_name="废弃物总量")
    # 单位营收废弃物总量
    waste_per_revenue = models.DecimalField(max_digits=20, decimal_places=2, verbose_name="单位营收废弃物总量")
    # 综合耗能
    energy_consumption = models.DecimalField(max_digits=20, decimal_places=2, verbose_name="综合耗能")
    # 单位营收综合耗能
    energy_consumption_per_revenue = models.DecimalField(max_digits=20, decimal_places=2,
                                                         verbose_name="单位营收综合耗能")
    # 环境保护投入
    environmental_protection_investment = models.DecimalField(max_digits=20, decimal_places=2,
                                                              verbose_name="环境保护投入")

    def __str__(self):
        return self.stock_name

    class Meta:
        verbose_name = "环境数据"
        verbose_name_plural = "环境数据"

#ESG评级

class Company(models.Model):
    serial_number = models.IntegerField(verbose_name='序号')
    name = models.CharField(max_length=100, verbose_name='公司名称')
    rating_date = models.DateField(verbose_name='评级日期')
    esg_rating = models.CharField(max_length=10, verbose_name='ESG级别')
    e_dimension_rating = models.CharField(max_length=10, verbose_name='E维度级别')
    s_dimension_rating = models.CharField(max_length=10, verbose_name='S维度级别')
    g_dimension_rating = models.CharField(max_length=10, verbose_name='G维度级别')
    province = models.CharField(max_length=50, verbose_name='所属省')
    city = models.CharField(max_length=50, verbose_name='所属市')
    district = models.CharField(max_length=50, verbose_name='所属县')
    industry = models.CharField(max_length=100, verbose_name='行业')
    establishment_date = models.DateField(verbose_name='成立日期')
    registered_capital = models.CharField(max_length=100, verbose_name='注册资本')

    def __str__(self):
        return self.name

#社会责任

class CompanyData(models.Model):
    # 序号
    serial_number = models.IntegerField(verbose_name='序号')
    # 股票简称
    stock_abbreviation = models.CharField(max_length=100, verbose_name='股票简称')
    # 股票代码
    stock_code = models.CharField(max_length=10, verbose_name='股票代码')
    # 报告期
    reporting_period = models.CharField(max_length=20, verbose_name='报告期')
    # 员工人数
    employee_count = models.IntegerField(verbose_name='员工人数', null=False, blank=True)
    # 研发费用(万元)
    r_and_d_expenses = models.DecimalField(max_digits=15, decimal_places=2, verbose_name='研发费用(万元)', null=True, blank=True)
    # 研发费用增长率(%)
    r_and_d_expenses_growth_rate = models.DecimalField(max_digits=15, decimal_places=2, verbose_name='研发费用增长率(%)', null=True, blank=True)
    # 研发费用占营收比(%)
    r_and_d_expenses_to_revenue_ratio = models.DecimalField(max_digits=15, decimal_places=2, verbose_name='研发费用占营收比(%)', null=True, blank=True)
    # 公益投入金额
    public_welfare_investment = models.DecimalField(max_digits=15, decimal_places=2, verbose_name='公益投入金额', null=True, blank=True)
    # 扶贫投入金额
    poverty_alleviation_investment = models.DecimalField(max_digits=15, decimal_places=2, verbose_name='扶贫投入金额', null=True, blank=True)
    # 员工薪酬总额
    total_employee_compensation = models.DecimalField(max_digits=15, decimal_places=2, verbose_name='员工薪酬总额', null=True, blank=True)
    # 员工人均薪酬
    average_employee_compensation = models.DecimalField(max_digits=15, decimal_places=2, verbose_name='员工人均薪酬', null=True, blank=True)

    def __str__(self):
        return self.stock_abbreviation


#公司治理

class ShareholderData(models.Model):
    # 序号
    serial_number = models.IntegerField(verbose_name='序号')
    # 股票简称
    stock_abbreviation = models.CharField(max_length=100, verbose_name='股票简称')
    # 股票代码
    stock_code = models.CharField(max_length=10, verbose_name='股票代码')
    # 报告期
    reporting_period = models.CharField(max_length=20, verbose_name='报告期')
    # 实际控制人名称
    actual_controller_name = models.CharField(max_length=255, verbose_name='实际控制人名称')
    # 实际控制人性质
    actual_controller_nature = models.CharField(max_length=20, verbose_name='实际控制人性质')
    # 股东人数(人)
    shareholder_count = models.IntegerField(verbose_name='股东人数(人)')
    # 第一大股东持股数量(万股)
    largest_shareholder_quantity = models.DecimalField(max_digits=15, decimal_places=2, verbose_name='第一大股东持股数量(万股)')
    # 第一大股东持股比例(%)
    largest_shareholder_percentage = models.DecimalField(max_digits=15, decimal_places=2, verbose_name='第一大股东持股比例(%)')
    # 前十大股东持股数量合计(万股)
    top_ten_shareholders_quantity = models.DecimalField(max_digits=15, decimal_places=2, verbose_name='前十大股东持股数量合计(万股)')
    # 前十大股东持股占比(%)
    top_ten_shareholders_percentage = models.DecimalField(max_digits=15, decimal_places=2, verbose_name='前十大股东持股占比(%)')
    # 总股份(万股)
    total_shares = models.DecimalField(max_digits=15, decimal_places=2, verbose_name='总股份(万股)')
    # 现金分红总额
    total_cash_dividends = models.DecimalField(max_digits=10, decimal_places=2)

    # 现金分红率
    cash_dividend_rate = models.DecimalField(max_digits=5, decimal_places=2)

    # 每股现金分红金额
    per_share_cash_dividend_amount = models.DecimalField(max_digits=5, decimal_places=2)

    # 董事会人数
    board_members_count = models.IntegerField(verbose_name='董事会人数')

    # 女性董事人数
    female_directors_count = models.IntegerField(verbose_name='女性董事人数')

    # 女性董事比例
    female_directors_percentage = models.DecimalField(max_digits=5, decimal_places=2)

    def __str__(self):
        return self.stock_abbreviation
#esg评分
class Securities(models.Model):
    security_code = models.CharField(max_length=20, verbose_name='证券代码')
    security_name = models.CharField(max_length=100, verbose_name='证券简称')
    overall_rating = models.CharField(max_length=10, verbose_name='综合评级')
    year = models.IntegerField(verbose_name='年度')
    overall_score = models.FloatField(verbose_name='综合得分')
    e_rating = models.CharField(max_length=10, verbose_name='E评级')
    e_score = models.FloatField(verbose_name='E得分')
    s_rating = models.CharField(max_length=10, verbose_name='S评级')
    s_score = models.FloatField(verbose_name='S得分')
    g_rating = models.CharField(max_length=10, verbose_name='G评级')
    g_score = models.FloatField(verbose_name='G得分')

    class Meta:
        verbose_name = '证券'
        verbose_name_plural = '证券'

    def __str__(self):
        return f'{self.security_code} - {self.security_name}'

#关注公司
class FollowedCompany(models.Model):
    id = models.AutoField(primary_key=True)
    user_id = models.IntegerField(verbose_name='用户ID')
    company_name = models.CharField(max_length=255, verbose_name='公司名称')
    follow_time = models.DateTimeField(auto_now_add=True, verbose_name='关注时间')

    class Meta:
        verbose_name = '关注公司'
        verbose_name_plural = '关注公司列表'

    def __str__(self):
        return f'{self.company_name} - {self.user_id}'