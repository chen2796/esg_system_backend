# Generated by Django 4.2.4 on 2024-03-10 05:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('esg_content', '0003_alter_companydata_employee_count'),
    ]

    operations = [
        migrations.AddField(
            model_name='shareholderdata',
            name='board_members_count',
            field=models.IntegerField(default=0, verbose_name='董事会人数'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='shareholderdata',
            name='cash_dividend_rate',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=5),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='shareholderdata',
            name='female_directors_count',
            field=models.IntegerField(default=0, verbose_name='女性董事人数'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='shareholderdata',
            name='female_directors_percentage',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=5),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='shareholderdata',
            name='per_share_cash_dividend_amount',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=5),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='shareholderdata',
            name='total_cash_dividends',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=10),
            preserve_default=False,
        ),
    ]
