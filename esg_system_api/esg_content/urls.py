from django.urls import path, include
from rest_framework.routers import DefaultRouter

from .views import (upload_excel, EnvironmentalDataList, EnvironmentalDataDetail,
                    CompanyList, SRatingCountView, OverallScoreByYearView, ERatingCountView, GRatingCountView,
                    CompanyDetail, CompanyDataList, CompanyDataDetail, ShareholderDataList, ShareholderDataDetail,
                    CompanyCountByESGView, OverallRatingCountView, TopOverallScoresView, CompanyByProvinceView,
                    EnvironmentalDataViewSet, CompanyViewSet, CompanyDataViewSet, ShareholderDataViewSet,
                    average_e_score_by_year, average_s_score_by_year, average_g_score_by_year, OverallRatingTopThree,
                    ERatingTopThree, SRatingTopThree, GRatingTopThree, OverallRatingTopSix, ERatingTopSix,
                    SRatingTopSix, GRatingTopSix, FollowedCompanyViewSet, UploadSecuritiesExcel,
                    user_followed_companies)


router = DefaultRouter()
router.register(r'environmental-data', EnvironmentalDataViewSet)
router.register(r'company', CompanyViewSet)
router.register(r'company-data', CompanyDataViewSet)
router.register(r'shareholder-data', ShareholderDataViewSet)
router.register(r'followed-companies', FollowedCompanyViewSet)
urlpatterns = [
    path('', include(router.urls)),
    path('upload_excel/', upload_excel, name='upload_excel'),

    path('environmental_data/', EnvironmentalDataList.as_view(), name='environmental_data_list'),
    path('environmental_data/<int:pk>/', EnvironmentalDataDetail.as_view(), name='environmental_data_detail'),

    # path('company/', CompanyList.as_view(), name='company_list'),
    # path('company/<int:pk>/', CompanyDetail.as_view(), name='company_detail'),

    path('company_data/', CompanyDataList.as_view(), name='company_data_list'),
    path('company_data/<int:pk>/', CompanyDataDetail.as_view(), name='company_data_detail'),

    path('shareholder_data/', ShareholderDataList.as_view(), name='shareholder_data_list'),
    path('shareholder_data/<int:pk>/', ShareholderDataDetail.as_view(), name='shareholder_data_detail'),

    path('company-count-by-esg/', CompanyCountByESGView.as_view(), name='company-count-by-esg'),
    path('overall-rating-count/', OverallRatingCountView.as_view(), name='overall-rating-count'),
    path('s-rating-count/', SRatingCountView.as_view(), name='s-rating-count'),
    path('e-rating-count/', ERatingCountView.as_view(), name='e-rating-count'),
    path('g-rating-count/', GRatingCountView.as_view(), name='g-rating-count'),
    path('overall-score-by-year/', OverallScoreByYearView.as_view(), name='overall-score-by-year'),
    path('top-overall-scores/', TopOverallScoresView.as_view(), name='top-overall-scores'),
    path('company-by-province/', CompanyByProvinceView.as_view(), name='company-by-province'),
    path('average-e-score-by-year/', average_e_score_by_year, name='average_e_score_by_year'),
    path('average-s-score-by-year/', average_s_score_by_year, name='average_s_score_by_year'),
    path('average-g-score-by-year/', average_g_score_by_year, name='average_g_score_by_year'),
    path('overall-top-three/', OverallRatingTopThree.as_view(), name='overall-top-three'),
    path('e-top-three/', ERatingTopThree.as_view(), name='e-top-three'),
    path('s-top-three/', SRatingTopThree.as_view(), name='s-top-three'),
    path('g-top-three/', GRatingTopThree.as_view(), name='g-top-three'),
    path('overall-top-six/', OverallRatingTopSix.as_view(), name='overall-top-six'),
    path('e-top-six/', ERatingTopSix.as_view(), name='e-top-six'),
    path('s-top-six/', SRatingTopSix.as_view(), name='s-top-six'),
    path('g-top-six/', GRatingTopSix.as_view(), name='g-top-six'),
    path('upload-excel-esg/', UploadSecuritiesExcel.as_view(), name='upload-excel'),
    path('followed/<int:user_id>/', user_followed_companies, name='user_followed_companies'),
]