from rest_framework import serializers
from .models import EnvironmentalData, Company, CompanyData, ShareholderData, Securities, FollowedCompany


class EnvironmentalDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = EnvironmentalData
        fields = '__all__'

class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = '__all__'

class CompanyGroupBySerializer(serializers.Serializer):
    province = serializers.CharField()
    count = serializers.IntegerField()
class CompanyDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = CompanyData
        fields = '__all__'

class ShareholderDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = ShareholderData
        fields = '__all__'

class SecuritiesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Securities
        fields = '__all__'

class FollowedCompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = FollowedCompany
        fields = '__all__'