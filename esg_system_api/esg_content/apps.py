from django.apps import AppConfig


class EsgContentConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'esg_content'
